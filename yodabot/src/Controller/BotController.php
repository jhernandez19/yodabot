<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BotController extends Controller {
    
    /**
     * @Route("/message", name="message")
     * @Method({"GET"})
     */
    public function message() {
        
        return $this->render('chat/chatBot.html.twig');
        
        
    }
    
    
    
    
    
    
}