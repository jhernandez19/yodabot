<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class endpointController extends Controller {
    
    /**
     * @Route("/auth", name="auth")
     * @Method({"GET"})
     */
    public function testConectionEndpoint() {
        
        $ch = curl_init();
        $url = "https://api.inbenta.io/v1/auth" ;
        $secret = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9qZWN0IjoieW9kYV9jaGF0Ym90X2VuIn0.anf_ eerFhoNq6J8b36_qbD4VqngX79-yyBKWih_eA1-HyaMe2skiJXkRNpyWxpjmpySYWzPGncwvlwz5Z RE7eg";
        
        $headers = array(
            "x-inbenta-key : nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY=",
            "Content-Type: application/json");
        $body = json_encode([ 'Content-Disposition' => 'form-data',
            'secret' => $secret
        ]);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // Don't forget to close handle.
        curl_close($ch);
        
        $results_array = json_decode($response);
        
        return $this->render('chat/endpointApi.html.twig',  array('results' => $results_array));
       
    }
    
    
    /**
     * @Route("/conv", name="conv")
     * @Method({"GET"})
     */
    public function requestConversation() {
        
        $accessToken = "";
        $result = $this->authentication();
        
        if (empty($result) != false)  {
            //retorna error
            return null;
        }
        
        if ($result->accessToken) {
            $accessToken = "Bearer " . $result->accessToken;
        }
        
        //dump($accessToken);
        
        $headers = array(
            "x-inbenta-key : nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY=",
            "Content-Type : application/json",
            "Authorization : " . $accessToken
        );
        
        $sideBubbleAttributes = ["SIDEBUBBLE_TEXT"];
        $answerAttributes = ["ANSWER_TEXT"];
        $answers = array( "sideBubbleAttributes" => $sideBubbleAttributes, "answerAttributes" => $answerAttributes, "skipLastCheckQuestion" => true, "maxOptions" =>  4, "maxRelatedContents" => 2);
        $forms = array("allowUserToAbandonForm" => true,  "errorRetries" => 2);
        $userInfo = array("browserInfo" => "browser information", "custom" => "custom user information");
        $tracking = array("userInfo" => $userInfo);
        $payload = array("answers" => $answers, "forms" => $forms, "tracking" => $tracking, "lang" => "es");
        $body = json_encode(array('payload' => $payload));
        
        
        $ch = curl_init();
        $url = "https://api-gce3.inbenta.io/prod/chatbot/v1/conversation" ;
        
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);
        
        curl_close($ch);
        
        $results_array = json_decode($response);
        
        return $this->render('chat/endpointApi.html.twig',  array('results' => $results_array));
        
    }
    
    
    public function requestAuthorization() {
        //         return new Response('<html><Body>Hellow mother fucker World</Body></html>');
        return null;
    }
    
    /**
     * @Route("/chat", name="chat")
     * @Method({"GET"})
     */
    public function chat() {
        //         return new Response('<html><Body>Hellow mother fucker World</Body></html>');
        return $this->render('chat/chatBot.html.twig');
    }
    
    /**
     * @Route("/postmessage", name="postmessage")
     * @Method({"POST"})
     */
    public function sendMessage(Request $request) {
        
        $message = $request->get('message');
        
        $sessionResults = $this->conversation();
        $authorization = $sessionResults['accessToken'];
        $conversation = $sessionResults['conversation'];
        $sessionId =$conversation->sessionId;
        $sessionToken = $conversation->sessionToken;
        
        $headers = array(
            "x-inbenta-key : nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY=",
            "Content-Type : application/json",
            "Authorization : " . $authorization,
            "x-inbenta-session : Bearer " . $sessionToken);
        
        $payload = array("message" => $message );
        
        $body = json_encode($payload);
        
        $ch = curl_init();
        $url = "https://api-gce3.inbenta.io/prod/chatbot/v1/conversation/message" ;
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        $results_array = json_decode($response);
        
        return new Response(json_encode($results_array));
    }
    
    private function authentication() {
      
        $ch = curl_init();
        $url = "https://api.inbenta.io/v1/auth" ;
        $secret = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9qZWN0IjoieW9kYV9jaGF0Ym90X2VuIn0.anf_ eerFhoNq6J8b36_qbD4VqngX79-yyBKWih_eA1-HyaMe2skiJXkRNpyWxpjmpySYWzPGncwvlwz5Z RE7eg";
        
        $headers = array(
            "x-inbenta-key : nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY=",
            "Content-Type: application/json");
        
        $body = [ 'Content-Disposition' => 'form-data',
                  'secret' => $secret ];
        
        $body = json_encode($body);
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);
        // Don't forget to close handle.
        curl_close($ch);
        
        $results_array = json_decode($response);
        
        return $results_array;
    }
    
    private function conversation() {
        
        $accessToken = "";
        $result = $this->authentication();
        
        if (empty($result) != false)  {
            //retorna error
            dump("ENTRA AQUI");
            return null;
        }
        
        if ($result->accessToken) {
            $accessToken = "Bearer " . $result->accessToken;
        }
        
        $headers = array(
            "x-inbenta-key : nyUl7wzXoKtgoHnd2fB0uRrAv0dDyLC+b4Y6xngpJDY=",
            "Content-Type : application/json",
            "Authorization : " . $accessToken
        );
        
        $sideBubbleAttributes = ["SIDEBUBBLE_TEXT"];
        $answerAttributes = ["ANSWER_TEXT"];
        $answers = array( "sideBubbleAttributes" => $sideBubbleAttributes, "answerAttributes" => $answerAttributes, "skipLastCheckQuestion" => true, "maxOptions" =>  4, "maxRelatedContents" => 2);
        $forms = array("allowUserToAbandonForm" => true,  "errorRetries" => 2);
        $userInfo = array("browserInfo" => "browser information", "custom" => "custom user information");
        $tracking = array("userInfo" => $userInfo);
        $payload = array("answers" => $answers, "forms" => $forms, "tracking" => $tracking, "lang" => "es");
        $body = json_encode(array('payload' => $payload));
        
        $ch = curl_init();
        $url = "https://api-gce3.inbenta.io/prod/chatbot/v1/conversation" ;
        
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);
        
        curl_close($ch);
        
        $results_array = json_decode($response);
        
        return array('conversation' => $results_array, 'accessToken' => $accessToken);
    }
    
    
}
